import logging
import sys

logger = logging.getLogger()
hdlr = logging.FileHandler('logs/python.log')
formatter = logging.Formatter('frontend %(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)


_old_excepthook = sys.excepthook


def myexcepthook(exctype, value, traceback):
    logging.getLogger().exception(value)
    _old_excepthook(exctype, value, traceback)

sys.excepthook = myexcepthook
